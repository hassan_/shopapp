import React, {useState} from 'react';
// Commands npm install --save expo-font  or expo install --save expo-font
import * as Font from 'expo-font';
import { AppLoading } from 'expo';


import {createStore, combineReducers, applyMiddleware } from 'redux';
import {Provider} from 'react-redux'; 

import productsReducer from './store/reducers/products';
import cartReducer from './store/reducers/cart';
import orderReducer from './store/reducers/order';
import authReducer from './store/reducers/auth';
// import ShopNavigator from './navigation/ShopNavigator';
import NavigationContainer from './navigation/NavigationContainer';
import ReduxThunk from 'redux-thunk';

const rootReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  orders: orderReducer,
  auth: authReducer
});
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans-Regular': require('./assets/fonts/ProductSansRegular.ttf'),
    'open-sans-Bold': require('./assets/fonts/ProductSansBold.ttf'),
    'open-sans-BoldItalic': require('./assets/fonts/ProductSansBoldItalic.ttf'),
    'open-sans-Italic': require('./assets/fonts/ProductSansItalic.ttf'),
  });
};


export default function App() {

  const [fontLoaded, setFontoaded] = useState(false);
  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontoaded(true)}
        onError={(err) => console.log(err)}
      />
    )
  }
  
  return (
    <Provider store={store}>
      <NavigationContainer />
    </Provider>
  );
}

